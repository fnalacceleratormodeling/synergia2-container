# synergia2-container

This repository generates a docker-container for running `synergia2` via the CI process using the docker-in-docker mechanism (the kaniko docker build fails to copy symlinks which are present in the spack-environment view, thus we do not use it). The docker recipe is generated using [`spack containerize`](https://spack.readthedocs.io/en/latest/containers.html). Note that the location of view has been modified for consistency with the default location (at `$SPACK_ENV/.spack-env/view`)!

The container can be pulled via `docker pull registry.gitlab.com/pirt/synergia2-container:latest` to experiment with `synergia2`. Note that this docker image is not built for performance, so the performance of `synergia2` obtained when using it is misleading. 

